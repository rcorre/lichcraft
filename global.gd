extends Node

enum Action {
	TAKE_ITEM,
	GIVE_ITEM,
	LEARN,
	SCRY,
	WAKE_SPECTRE,
}
