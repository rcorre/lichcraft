extends Object

const MANA_COST = 25
const ICON = preload("icon.svg")
const COOLDOWN_TIME = 2.0

var cooldown : float

func set_theme(mat: Material, col: Color):
	pass

func cast(caster: Node, world: Node, trans: Transform):
	if caster.mana < MANA_COST or cooldown > 0:
		return

	var area := caster.get_node("TeleportArea") as Area
	if not area.get_overlapping_bodies():
		caster.global_transform.origin = area.global_transform.origin

func update(caster: Node, delta: float):
	cooldown = max(0, cooldown - delta)

func cooldown_factor() -> float:
	return cooldown / COOLDOWN_TIME
