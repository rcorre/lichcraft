extends Object

const MANA_COST := 30
const DAMAGE := 30.0
const VELOCITY := Vector3(0, 0, 10)
const COOLDOWN_TIME := 1.5
const PROJECTILE_COUNT := 5
const SPREAD := 60.0
const PROJECTILE := preload("projectile.tscn")
const ICON := preload("icon.svg")

var theme_material : Material
var theme_color : Color
var cooldown : float

func set_theme(mat: Material, col: Color):
	theme_material = mat
	theme_color = col

func cast(caster: Node, world: Node, trans: Transform):
	if caster.mana < MANA_COST or cooldown > 0:
		return

	caster.mana -= MANA_COST
	cooldown = COOLDOWN_TIME

	for i in range(PROJECTILE_COUNT):
		var angle := deg2rad((i * SPREAD / (PROJECTILE_COUNT - 1)) - SPREAD / 2)
		var p = PROJECTILE.instance()
		p.set_theme(theme_material, theme_color)
		world.add_child(p)
		p.global_transform = trans
		p.linear_velocity = trans.basis.xform(VELOCITY).rotated(Vector3.UP, angle) + caster.get_floor_velocity()
		p.power = DAMAGE

func update(caster: Node, delta: float):
	cooldown = max(0, cooldown - delta)

func cooldown_factor() -> float:
	return cooldown / COOLDOWN_TIME
