extends RigidBody

var power : float

func _ready():
	$Area.connect("body_entered", self, "_on_body_entered")

func _on_body_entered(body):
	if body.has_method("projectile_hit") and not body.ghosted:
		queue_free()
		if is_network_master():
			body.rpc("projectile_hit", power)

func set_theme(mat: Material, col: Color):
	$Particles.material_override = mat
