extends RigidBody

const LOCKON_RANGE := 20.0
const ACCEL := 5.0

var power : float
var caster : Node

func _ready():
	$Area.connect("body_entered", self, "_on_body_entered")

func _physics_process(delta):
	for lich in get_tree().get_nodes_in_group("lich"):
		var disp = lich.global_transform.origin - global_transform.origin + Vector3(0, 0.5, 0)
		if lich != caster and disp.length() < LOCKON_RANGE:
			add_central_force(disp.normalized() * 2000.0 * delta)

func _on_body_entered(body):
	queue_free()
	if body.has_method("projectile_hit") and not body.ghosted:
		if is_network_master():
			body.rpc("projectile_hit", power)

func set_theme(mat: Material, col: Color):
	$Particles.material_override = mat
