extends Object

const MANA_COST_PER_SECOND = 15
const ICON = preload("icon.svg")

var active := false

func set_theme(mat: Material, col: Color):
	pass

func cast(caster: Node, world: Node, trans: Transform):
	active = not active

func update(caster: Node, delta: float):
	caster.condition.haste = 0
	if active:
		var cost := MANA_COST_PER_SECOND * delta
		if caster.mana < cost:
			active = false
		else:
			caster.mana -= cost
			caster.condition.haste = 1

func cooldown_factor() -> float:
	return 1.0 if active else 0.0
