extends RigidBody

var power : float

func _physics_process(delta):
	var body = $RayCast.get_collider()
	if body:
		queue_free()
		if is_network_master() and body.has_method("projectile_hit") and not body.ghosted:
			body.rpc("projectile_hit", power)

func set_theme(mat: Material, col: Color):
	$Particles.material_override = mat
	$MeshInstance.material_override = mat
	$OmniLight.light_color = col
