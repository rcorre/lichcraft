extends Object

const MANA_COST = 15
const DAMAGE = 30.0
const VELOCITY = Vector3(0, 0, 40)
const COOLDOWN_TIME = 0.5
const PROJECTILE = preload("projectile.tscn")
const ICON = preload("icon.svg")

var theme_material : Material
var theme_color : Color
var cooldown : float

func set_theme(mat: Material, col: Color):
	theme_material = mat
	theme_color = col

func cast(caster: Node, world: Node, trans: Transform):
	if caster.mana < MANA_COST or cooldown > 0:
		return

	caster.mana -= MANA_COST
	var p = PROJECTILE.instance()
	p.set_theme(theme_material, theme_color)
	world.add_child(p)
	p.global_transform = trans
	p.linear_velocity = trans.basis.xform(VELOCITY)
	p.power = DAMAGE
	cooldown = COOLDOWN_TIME

func update(caster: Node, delta: float):
	cooldown = max(0, cooldown - delta)

func cooldown_factor() -> float:
	return cooldown / COOLDOWN_TIME
