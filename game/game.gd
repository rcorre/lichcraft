extends Node

const PORT_FORWARD_DURATION = 3600

const UPNP_ERROR = {
	UPNP.UPNP_RESULT_SUCCESS: "UPNP_RESULT_SUCCESS",
	UPNP.UPNP_RESULT_NOT_AUTHORIZED: "UPNP_RESULT_NOT_AUTHORIZED",
	UPNP.UPNP_RESULT_PORT_MAPPING_NOT_FOUND: "UPNP_RESULT_PORT_MAPPING_NOT_FOUND",
	UPNP.UPNP_RESULT_INCONSISTENT_PARAMETERS: "UPNP_RESULT_INCONSISTENT_PARAMETERS",
	UPNP.UPNP_RESULT_NO_SUCH_ENTRY_IN_ARRAY: "UPNP_RESULT_NO_SUCH_ENTRY_IN_ARRAY",
	UPNP.UPNP_RESULT_ACTION_FAILED: "UPNP_RESULT_ACTION_FAILED",
	UPNP.UPNP_RESULT_SRC_IP_WILDCARD_NOT_PERMITTED: "UPNP_RESULT_SRC_IP_WILDCARD_NOT_PERMITTED",
	UPNP.UPNP_RESULT_EXT_PORT_WILDCARD_NOT_PERMITTED: "UPNP_RESULT_EXT_PORT_WILDCARD_NOT_PERMITTED",
	UPNP.UPNP_RESULT_INT_PORT_WILDCARD_NOT_PERMITTED: "UPNP_RESULT_INT_PORT_WILDCARD_NOT_PERMITTED",
	UPNP.UPNP_RESULT_REMOTE_HOST_MUST_BE_WILDCARD: "UPNP_RESULT_REMOTE_HOST_MUST_BE_WILDCARD",
	UPNP.UPNP_RESULT_EXT_PORT_MUST_BE_WILDCARD: "UPNP_RESULT_EXT_PORT_MUST_BE_WILDCARD",
	UPNP.UPNP_RESULT_NO_PORT_MAPS_AVAILABLE: "UPNP_RESULT_NO_PORT_MAPS_AVAILABLE",
	UPNP.UPNP_RESULT_CONFLICT_WITH_OTHER_MECHANISM: "UPNP_RESULT_CONFLICT_WITH_OTHER_MECHANISM",
	UPNP.UPNP_RESULT_CONFLICT_WITH_OTHER_MAPPING: "UPNP_RESULT_CONFLICT_WITH_OTHER_MAPPING",
	UPNP.UPNP_RESULT_SAME_PORT_VALUES_REQUIRED: "UPNP_RESULT_SAME_PORT_VALUES_REQUIRED",
	UPNP.UPNP_RESULT_ONLY_PERMANENT_LEASE_SUPPORTED: "UPNP_RESULT_ONLY_PERMANENT_LEASE_SUPPORTED",
	UPNP.UPNP_RESULT_INVALID_GATEWAY: "UPNP_RESULT_INVALID_GATEWAY",
	UPNP.UPNP_RESULT_INVALID_PORT: "UPNP_RESULT_INVALID_PORT",
	UPNP.UPNP_RESULT_INVALID_PROTOCOL: "UPNP_RESULT_INVALID_PROTOCOL",
	UPNP.UPNP_RESULT_INVALID_DURATION: "UPNP_RESULT_INVALID_DURATION",
	UPNP.UPNP_RESULT_INVALID_ARGS: "UPNP_RESULT_INVALID_ARGS",
	UPNP.UPNP_RESULT_INVALID_RESPONSE: "UPNP_RESULT_INVALID_RESPONSE",
	UPNP.UPNP_RESULT_INVALID_PARAM: "UPNP_RESULT_INVALID_PARAM",
	UPNP.UPNP_RESULT_HTTP_ERROR: "UPNP_RESULT_HTTP_ERROR",
	UPNP.UPNP_RESULT_SOCKET_ERROR: "UPNP_RESULT_SOCKET_ERROR",
	UPNP.UPNP_RESULT_MEM_ALLOC_ERROR: "UPNP_RESULT_MEM_ALLOC_ERROR",
	UPNP.UPNP_RESULT_NO_GATEWAY: "UPNP_RESULT_NO_GATEWAY",
	UPNP.UPNP_RESULT_NO_DEVICES: "UPNP_RESULT_NO_DEVICES",
	UPNP.UPNP_RESULT_UNKNOWN_ERROR: "UPNP_RESULT_UNKNOWN_ERROR"
}

func _ready():
	get_tree().connect("connected_to_server", self, "_on_connected_to_server")
	get_tree().connect("connection_failed", self, "_on_connection_failed")
	get_tree().connect("network_peer_disconnected", self, "_on_network_peer_disconnected")
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")

func _on_connected_to_server():
	rpc("begin")

func _on_connection_failed():
	pass

func _on_network_peer_disconnected(id):
	pass

func _on_server_disconnected():
	pass

func host(port):
	var host = NetworkedMultiplayerENet.new()
	host.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	var err = host.create_server(port, 2)
	if err != OK:
		print("Can't host, address in use.")
		return
	get_tree().set_network_peer(host)

	#var upnp := UPNP.new()
	#err = upnp.discover()
	#if not err:
	#	return upnp.query_external_address()
	return "???"

func join(address, port):
	var host = NetworkedMultiplayerENet.new()
	host.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	host.create_client(address, port)
	get_tree().set_network_peer(host)

remotesync func begin():
	var menu = get_child(0)
	remove_child(menu)
	menu.queue_free()

	var level = load("res://levels/crypt.tscn").instance()
	add_child(level)
	for id in get_tree().get_network_connected_peers():
		level.spawn_player(id)
	level.spawn_player(get_tree().get_network_unique_id())

	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func port_forward(port):
	var upnp := UPNP.new()
	var err := upnp.discover()
	if err:
		return UPNP_ERROR[err]

	err = _forward_protocol(upnp, port, "UDP")
	if err:
		return UPNP_ERROR[err]

	err = _forward_protocol(upnp, port, "TCP")
	if err:
		return UPNP_ERROR[err]

	return ""

func _forward_protocol(upnp, port, proto):
	var err = upnp.add_port_mapping(port, port, "LichCraft multiplayer %s" % proto, proto, PORT_FORWARD_DURATION)
	if err:
		# try without description and duration, as these are unsupported on some routers
		err = upnp.add_port_mapping(port, port, "", proto)
	return err

func stop_port_forward(port):
	var upnp := UPNP.new()
	var err := upnp.discover()
	if err:
		return UPNP_ERROR[err]

	err = upnp.delete_port_mapping(port, "UDP")
	if err:
		return UPNP_ERROR[err]

	err = upnp.delete_port_mapping(port, "TCP")
	if err:
		return UPNP_ERROR[err]
