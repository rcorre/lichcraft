extends Navigation
class_name Level

func _ready():
	var players = Spatial.new()
	players.name = "Players"
	add_child(players)

func spawn_player(id):
	var player = preload("res://lich/lich.tscn").instance()
	player.name = String(id)
	$Players.add_child(player)

	if id == 1:
		player.set_theme(preload("res://lich/green_mana.material"), Color(0, 1, 0))
		player.global_transform = $SpawnPoints.get_node("Host").global_transform
	else:
		player.set_theme(preload("res://lich/red_mana.material"), Color(1, 0, 0))
		player.global_transform = $SpawnPoints.get_node("Guest").global_transform

	player.init()
