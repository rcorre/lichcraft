extends Control

const HintIcon = {
	CLOSE = preload("res://lich/hud/icons/close.svg"),
	OPEN = preload("res://lich/hud/icons/open.svg")
}

onready var guide_arrow = $Reticle/Control/GuideArrow
onready var crosshairs = $Reticle/Crosshairs
onready var spellslots = $ManaPanel/SpellSlots.get_children()

var draining := false

func _process(delta: float):
	var drain_delta := delta * (1 if draining else -1)
	$DrainIndicator.modulate.a = clamp($DrainIndicator.modulate.a + drain_delta, 0, 1)

func set_mana(val):
	$ManaPanel/ManaBar.value = val

func show_hint(hint):
	$Hint.visible = true
	$Hint/Label.text = hint

func hide_hint():
	$Hint.visible = false

func show_endgame(win):
	if win:
		$VictoryLabel.visible = true
	else:
		$DefeatLabel.visible = true

func show_crosshairs():
	crosshairs.visible = true
	guide_arrow.visible = false

func show_guide():
	crosshairs.visible = false
	guide_arrow.visible = true

func set_guide_angle(angle):
	guide_arrow.rect_rotation = rad2deg(angle)

func set_color(col: Color):
	$ManaPanel/ManaBar.tint_progress = col
	for s in spellslots:
		s.tint_under = col

func set_cooldown(idx: int, val: float):
	spellslots[idx].value = val * 100.0

func set_spell_icon(idx: int, icon):
	spellslots[idx].texture_under = icon
