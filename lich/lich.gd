extends KinematicBody

const MAX_MANA := 100.0
const MANA_REGEN := 10.0
const MANA_CHARGE := 20.0
const RESPAWN_RANGE := 5.0
const GRAVITY := Vector3(0, -9.8, 0)
const MOUSE_SENSITIVITY := Vector2(0.05, 0.1)
const CONDITION_RECOVERY := 0.2
const LEARN_MANA_COST := 50.0
const SCRY_MANA_COST := 50.0
const AWAKEN_MANA_COST := 50.0

var spells = [
	preload("res://spells/arrow/spell.gd").new(),
	preload("res://spells/blink/blink.gd").new(),
	preload("res://spells/sparks/sparks.gd").new()
]

onready var anim_tree = $AnimationTree
onready var cast_point = $Armature/HeadBoneAttachment/Camera/CastPoint
onready var interact_ray = $Armature/HeadBoneAttachment/Camera/InteractRay
onready var ghost_material = preload("res://lich/ghost.material")

var theme_material : Material
var theme_color : Color

# input vars
var look_target := 0.0
var turn_target := 0.0

# state vars
var velocity := Vector3()
var mana := MAX_MANA
var ghosted := false
var move_speed := 1.0
var knowledge := 0
var phylactery

var condition := {
	haste = 0.0,
}
var draining := false

# puppet vars
var is_local : bool
sync var move_target := Vector2(0, 0)
sync var look_amount := 0.0
sync var turn_amount := 0.0
puppet var puppet_pos : Vector3

func init():
	is_local = name == String(get_tree().get_network_unique_id())
	puppet_pos = global_transform.origin
	$HUD.visible = is_local
	$Armature/HeadBoneAttachment/Camera.current = is_local
	phylactery = cast_point.get_child(0)

	$HUD.show_hint("Hide your phylactery!")

func _physics_process(delta: float):
	if is_local:
		# locally controlled
		rset("move_target", Vector2(
			Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
			Input.get_action_strength("move_forward") - Input.get_action_strength("move_backward")
		))
		rset("look_amount", clamp(look_amount + look_target * delta, -1, 1))
		rset("turn_amount", fmod(turn_amount + turn_target * delta, 2 * PI))
		look_target = 0.0
		turn_target = 0.0

	rotation.y = turn_amount
	anim_tree["parameters/move/blend_position"] = move_target
	anim_tree["parameters/look/add_amount"] = look_amount
	anim_tree["parameters/move_speed/scale"] = 2 if ghosted else (1 + condition.haste)

	if is_network_master():
		rset_unreliable("puppet_pos", global_transform.origin)
	else:
		global_transform.origin = puppet_pos

	var root_motion = anim_tree.get_root_motion_transform()
	var vel = global_transform.basis.xform(root_motion.origin) / delta
	velocity.x = vel.x
	velocity.z = vel.z
	velocity += GRAVITY * delta
	velocity = move_and_slide(velocity, Vector3(0, 1, 0))

	puppet_pos = global_transform.origin

	if not ghosted:
		mana = clamp(mana + MANA_REGEN * delta, 0, MAX_MANA)

	if ghosted:
		var disp = phylactery.global_transform.origin - global_transform.origin
		var fwd = Vector2(global_transform.basis.z.z, global_transform.basis.z.x)
		$HUD.set_guide_angle(Vector2(disp.z, disp.x).angle_to(fwd))
		if disp.length() < RESPAWN_RANGE:
			mana = clamp(mana + MANA_CHARGE * delta, 0, MAX_MANA)
			if mana == MAX_MANA and is_network_master():
				rpc("restore_life")

	var container = interact_ray.get_collider()
	if container and container.has_method("set_looked_at"):
		container.set_looked_at()

	for c in condition:
		var val = condition[c]
		condition[c] = clamp(
			val - CONDITION_RECOVERY * sign(val) * delta,
			-1.0, 1.0
		)

	for s in spells:
		s.update(self, delta)

	$HUD.draining = draining
	draining = false

func _process(delta):
	$HUD.set_mana(mana)
	for i in len(spells):
		var cooldown_factor = spells[i].cooldown_factor() if i <= knowledge else 1.0
		$HUD.set_cooldown(i, cooldown_factor)

func _input(event):
	if not is_local:
		return

	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		turn_target -= event.relative.x * MOUSE_SENSITIVITY.x
		look_target -= event.relative.y * MOUSE_SENSITIVITY.y
	elif event.is_action_pressed("cast1") and not ghosted:
		var body = interact_ray.get_collider()
		if body:
			rpc("interact")
		else:
			rpc("request_cast", 0)
	elif event.is_action_pressed("cast2") and not ghosted and knowledge > 0:
		rpc("request_cast", 1)
	elif event.is_action_pressed("cast3") and not ghosted and knowledge > 1:
		rpc("request_cast", 2)

func valid_sender():
	var sender := get_tree().get_rpc_sender_id()
	var player := int(name)
	if sender == 0:
		# local calls always allowed
		return true
	elif is_network_master() and sender != player:
		print("Player %d tried to send RPC for %d" % [sender, player])
		print_stack()
		return false
	elif not is_network_master() and sender != 1:
		print("Player %d got RPC from non-server %d" % [player, sender])
		print_stack()
		return false
	return true

func held_item():
	return cast_point.get_child(0) if cast_point.get_child_count() > 0 else null

master func interact():
	if not valid_sender():
		return

	var body = interact_ray.get_collider()
	if body and body.has_method("interact_action"):
		match body.interact_action(self):
			Global.Action.TAKE_ITEM:
				rpc("reparent", body.item_path(), cast_point.get_path())
			Global.Action.GIVE_ITEM:
				rpc("reparent", held_item().get_path(), body.storage_path())
			Global.Action.LEARN:
				if mana >= LEARN_MANA_COST and knowledge < len(spells):
					rpc("learn", body.get_path(), knowledge + 1)
			Global.Action.SCRY:
				if mana >= SCRY_MANA_COST:
					rpc("scry", body.get_path())
			Global.Action.WAKE_SPECTRE:
				if mana >= AWAKEN_MANA_COST:
					rpc("awaken_spectre", body.get_path())

remotesync func learn(tome: NodePath, new_knowledge: int):
	if not valid_sender():
		return

	mana -= LEARN_MANA_COST
	knowledge = new_knowledge
	get_node(tome).disable()

remotesync func scry(mirror: NodePath):
	if not valid_sender():
		return

	mana -= SCRY_MANA_COST
	get_node(mirror).enable()

remotesync func awaken_spectre(tomb: NodePath):
	if not valid_sender():
		return

	mana -= AWAKEN_MANA_COST
	get_node(tomb).activate(self)

remotesync func reparent(node_path, parent_path):
	if not valid_sender():
		return

	var node = get_node(node_path)
	var parent = get_node(parent_path)
	node.get_parent().remove_child(node)
	parent.add_child(node)
	node.transform = Transform()

#	if item == phylactery:
#		$HUD.show_hint("Hide your phylactery!")
#	else:
#		end_game(true)

master func request_cast(spell_idx: int):
	if valid_sender() and not held_item():
		rpc("cast", spell_idx)

remotesync func cast(spell_idx: int):
	if not valid_sender():
		return

	var spell = spells[spell_idx]
	spell.cast(self, get_parent(), cast_point.global_transform)

remotesync func projectile_hit(power):
	if not valid_sender():
		return

	mana -= power
	if is_network_master() and mana <= 0:
		rpc("lose_life")

remotesync func lose_life():
	if not valid_sender():
		return

	if held_item():
		end_game(false)

	$HUD.show_hint("Return to your phylactery!")
	$HUD.show_guide()
	ghosted = true
	$Armature/Cube.material_override = ghost_material
	$GhostTrail.emitting = true
	knowledge = 0

remotesync func restore_life():
	if not valid_sender():
		return

	$HUD.hide_hint()
	$HUD.show_crosshairs()
	ghosted = false
	$Armature/Cube.material_override = null
	$GhostTrail.emitting = false

func end_game(win):
	for lich in get_tree().get_nodes_in_group("lich"):
		if lich == self:
			lich.get_node("HUD").show_endgame(win)
		else:
			lich.get_node("HUD").show_endgame(not win)

func set_theme(mat: Material, col: Color):
	theme_material = mat
	theme_color = col

	# material 2 is the eyes
	$Armature/Cube.set_surface_material(2, mat)
	$SpotLight.light_color = col
	$Armature/HeadBoneAttachment/Camera/CastPoint/Phylactery.set_theme(mat, col)
	$HUD.set_color(col)

	for i in len(spells):
		var s = spells[i]
		$HUD.set_spell_icon(i, s.ICON)
		s.set_theme(mat, col)
