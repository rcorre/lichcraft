extends Area

var creator : Node
var power : float

func _ready():
	connect("body_entered", self, "_on_body_entered")

func _on_body_entered(body):
	if body != creator and body.has_method("projectile_hit"):
		$AnimationPlayer.play("explode")
		if is_network_master():
			body.rpc("projectile_hit", power)

func set_theme(mat: Material, col: Color):
	$Particles.material_override = mat
	$Explosion.material_override = mat
	$SpotLight.light_color = col
