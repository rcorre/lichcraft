extends KinematicBody

signal killed

const MAX_MANA := 100.0
const GRAVITY := Vector3(0, -9.8, 0)
const CONDITION_RECOVERY := 0.2
const MIN_TARGET_RANGE := 3.0
const ATTACK_RANGE := 6.0
const MANA_DRAIN := 20.0
const MOVE_SPEED := 0.5

onready var anim_tree := $AnimationTree as AnimationTree
onready var level := get_tree().get_nodes_in_group("level")[0] as Navigation

sync var turn_amount := 0.0
puppet var puppet_pos : Vector3

var theme_material : Material
var theme_color : Color
var velocity := Vector3()
var ghosted := false
var mana := MAX_MANA
var condition := {
	haste = 0.0,
}
var target : Spatial

var path : PoolVector3Array

func update_path():
	if target and not target.ghosted:
		path = level.get_simple_path(
			level.get_closest_point(level.to_local(global_transform.origin)),
			level.get_closest_point(level.to_local(target.global_transform.origin))
		)
	var im = get_tree().get_nodes_in_group("path_debug")[0]
	im.clear()
	im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
	for p in path:
		im.add_vertex(p)
	im.end()
	im.begin(Mesh.PRIMITIVE_POINTS, null)
	for p in path:
		im.add_vertex(p)
	im.end()

func _physics_process(delta: float):
	if not target or target.ghosted:
		return
	var move_target := Vector2()
	var disp := target.global_transform.origin - global_transform.origin
	if disp.length() > MIN_TARGET_RANGE:
		if path:
			var next := level.to_global(path[0])
			next.y = global_transform.origin.y
			if next.distance_to(global_transform.origin) < 0.1:
				path.remove(0)
			else:
				look_at(next, Vector3.UP)
				rotate_y(PI)
				move_target = Vector2(0, MOVE_SPEED)
	if disp.length() < ATTACK_RANGE:
		look_at(target.global_transform.origin, Vector3.UP)
		rotate_y(PI)
		$DrainEffect.global_transform.origin = target.global_transform.origin
		$DrainEffect/Particles.emitting = true
		target.draining = true
		target.mana -= MANA_DRAIN * delta
		if target.mana <= 0:
			target.rpc("lose_life")
	else:
		$DrainEffect/Particles.emitting = false

	anim_tree["parameters/move/blend_position"] = move_target

	if is_network_master():
		rset_unreliable("puppet_pos", global_transform.origin)
	else:
		global_transform.origin = puppet_pos

	var root_motion = anim_tree.get_root_motion_transform()
	var vel = global_transform.basis.xform(root_motion.origin) / delta
	velocity.x = vel.x
	velocity.z = vel.z
	velocity += GRAVITY * delta
	velocity = move_and_slide(velocity, Vector3(0, 1, 0))

	puppet_pos = global_transform.origin

	for c in condition:
		var val = condition[c]
		condition[c] = clamp(
			val - CONDITION_RECOVERY * sign(val) * delta,
			-1.0, 1.0
		)

func valid_sender():
	var sender := get_tree().get_rpc_sender_id()
	if sender != 0 and sender != 1:
		prints("Unexpected RPC from", sender)
		print_stack()
		return false
	return true

remotesync func projectile_hit(power):
	if not valid_sender():
		return

	mana -= power
	if is_network_master() and mana <= 0:
		rpc("lose_life")

remotesync func lose_life():
	if not valid_sender():
		return

	target = null
	emit_signal("killed")
	queue_free()

func set_theme(mat: Material, col: Color):
	theme_material = mat
	theme_color = col

	# material 2 is the eyes
	$Armature/Cube.set_surface_material(2, mat)
