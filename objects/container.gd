extends Spatial

signal open
signal close

var is_open := false
var is_looked_at := false

func _physics_process(delta):
	if not is_looked_at and is_open:
		close()
	elif is_looked_at and not is_open:
		open()

	is_looked_at = false

func set_looked_at():
	is_looked_at = true

func interact_action(user):
	var item
	if $StoragePoint.get_child_count() > 0:
		item = $StoragePoint.get_child(0)

	if item and not user.held_item():
		return Global.Action.TAKE_ITEM
	elif not item and user.held_item():
		return Global.Action.GIVE_ITEM

func close():
	is_open = false
	emit_signal("close")

func open():
	is_open = true
	emit_signal("open")

func storage_path():
	return $StoragePoint.get_path()

func item_path():
	return $StoragePoint.get_child(0).get_path()
