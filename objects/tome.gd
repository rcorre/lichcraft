extends Spatial

const RESPAWN_SECONDS = 20.0

var disabled := false

func interact_action(user):
	return null if disabled else Global.Action.LEARN

func set_looked_at():
	pass

func disable():
	disabled = true
	$LearnSound.play()
	$Particles.emitting = false
	yield(get_tree().create_timer(RESPAWN_SECONDS), "timeout")
	enable()

func enable():
	disabled = false
	$Particles.emitting = true
