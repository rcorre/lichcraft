extends StaticBody

const SCRY_DURATION = 8.0

onready var camera = $Viewport/Camera
onready var scry_camera = get_tree().get_nodes_in_group("scry_camera")[0]

var scrying := false

func _ready():
	disable()

func interact_action(user):
	return null if scrying else Global.Action.SCRY

func set_looked_at():
	pass

func disable():
	scrying = false
	camera.global_transform = $CameraTrans.global_transform

func enable():
	scrying = true
	camera.global_transform = scry_camera.global_transform
	yield(get_tree().create_timer(SCRY_DURATION), "timeout")
	disable()
