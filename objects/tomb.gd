extends StaticBody

export(PackedScene) var spectre_scene

onready var level := get_tree().get_nodes_in_group("level")[0] as Node
onready var anim_player := $Lid/AnimationPlayer as AnimationPlayer
onready var open_sound := $OpenSound as AudioStreamPlayer3D

var opened := false

func _ready():
	pass # Replace with function body.

func interact_action(user: Node):
	if not opened:
		return Global.Action.WAKE_SPECTRE

func activate(user: Node):
	opened = true
	open_sound.play()
	anim_player.play("Open")

	var spectre := spectre_scene.instance() as Spatial
	spectre.global_transform.origin = $SpawnPoint.global_transform.origin
	spectre.connect("killed", self, "deactivate")
	level.add_child(spectre)
	for l in get_tree().get_nodes_in_group("lich"):
		if l != user:
			spectre.target = l

func deactivate():
	opened = false
	open_sound.play()
	anim_player.play("Open")
