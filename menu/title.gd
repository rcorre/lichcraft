extends Control

onready var game = get_node("..")
onready var err_dialog = $ErrorDialog
onready var port_forward_button = $HBoxContainer/VBoxContainer/PortForwardButton

var address = "127.0.0.1" setget set_address
var port = "5846" setget set_port

func set_port(val):
	port = val

func set_address(val):
	address = val

func _on_host_pressed():
	var external_address = game.host(int(port))
	$HostPopup/CenterContainer/VBoxContainer/AddressLabel.text = "Hosting at %s:%s" % [external_address, port]

func _on_join_pressed():
	game.join(address, int(port))

func _on_port_forward_toggled(on: bool):
	var err
	if on:
		err = game.port_forward(int(port))
	else:
		err = game.stop_port_forward(int(port))

	if err:
		port_forward_button.pressed = false
		err_dialog.dialog_text = err
		err_dialog.popup()

func open_link(url):
	OS.shell_open(url)
