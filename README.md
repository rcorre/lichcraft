# Lich

My entry for [GoNetPlay 3](https://itch.io/jam/gonetplay-3)

# Assets

Any assets not explicitly listed below were created by @rcorre and are licensed
under [CC-By-4.0](https://creativecommons.org/licenses/by/4.0/).

## game-icons.net

- https://game-icons.net/1x1/lorc/plasma-bolt.html
- https://game-icons.net/1x1/lorc/sprint.html
- https://game-icons.net/1x1/lorc/laser-sparks.html
- https://game-icons.net/1x1/lorc/striking-arrows.html
- https://game-icons.net/1x1/lorc/teleport.html

## freesound.org

- https://freesound.org/people/Breviceps/sounds/457529/ (CC0)
- https://freesound.org/people/Lightnessko/sounds/390323/ (CC0)
- https://freesound.org/people/Milabrya/sounds/57673/ (CC0)
- https://freesound.org/people/HorrorAudio/sounds/431979/ (CC0) 

## fontlibrary.org

- [`common/medio.otf`](https://fontlibrary.org/en/font/medio) (CC0)
